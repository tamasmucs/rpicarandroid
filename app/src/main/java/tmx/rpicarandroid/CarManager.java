package tmx.rpicarandroid;

import android.graphics.Bitmap;

import tmx.rpicarcommon.Direction;
import tmx.rpicarcommon.EnvironmentData;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class CarManager {
    private  MotorManager mManager;
    private String rpiAddress;
    private Thread mThread; //motor
    private EnvironmentManager eManager;
    private Thread eThread;
    private VideoManager vManager;
    private Thread vThread;

    public CarManager(String rpiAddress) {
        this.rpiAddress =rpiAddress;
        //initialize MotorManager
        mManager = new MotorManager(this.rpiAddress);
        //Make a thread for the Motor commands
        mThread = new Thread(mManager);
        //Initialize EnvironmentManager
        eManager = new EnvironmentManager(this.rpiAddress);
        //Makea thread for collecting EnvironmentData
        eThread = new Thread (eManager);
        //init VideoManager
        vManager = new VideoManager(this.rpiAddress,false);
        //And a thread for the snapshots
        vThread = new Thread (vManager);
    }

    public void setDirection(Direction d){
        this.mManager.setDirection(d);
    }
    
    public EnvironmentData getCurrentEnvironmentData(){
        return this.eManager.getCurrentEnvironmentData();
    }

    public Bitmap getCurrentSnapshot(){
        return vManager.getCurrentSnapshot();
    }

    public void startFunctionalities(){
        //Start MotorManager for sending movement commands when needed.
        mThread.start();
        //Start EnvironmentManager to collect EnvironmentData
        eThread.start();
        //Start VideoManager to collect snapshots
        vThread.start();

    }
}