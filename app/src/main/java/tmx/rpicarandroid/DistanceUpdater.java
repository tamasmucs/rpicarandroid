package tmx.rpicarandroid;

import android.widget.TextView;


public class DistanceUpdater implements Runnable{

    private TextView distTextView;
    private double currentDistance = 0;
    private CarManager cManager;

    public DistanceUpdater(TextView distTextView, CarManager cManager) {
        this.distTextView = distTextView;
        this.cManager = cManager;
    }

    @Override
    public void run(){
        updateCurrentDistance();
        String distanceText= String.format("%.2f cm", currentDistance);
        distTextView.setText(distanceText);
        distTextView.postDelayed(this, 1000);
    }

    private void updateCurrentDistance(){
        try {
            this.currentDistance = cManager.getCurrentEnvironmentData().getDistance();
        }catch(NullPointerException ex){
            this.currentDistance = 0;
            System.out.println("NullPointer @ setting distance.");
        }
    }
}
