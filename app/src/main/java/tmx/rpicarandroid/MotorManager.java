/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tmx.rpicarandroid;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import tmx.rpicarcommon.CarComponent;
import tmx.rpicarcommon.Direction;
import java.net.InetAddress;

/**
 *
 * @author tamas
 */
public class MotorManager extends CarComponent implements Runnable{

    private Direction currentDirection;
    
    public synchronized Direction getCurrentDirection() {
        return currentDirection;
    }
  

    public MotorManager(String rpiAddress) {
        super(rpiAddress, 2175, false);
        currentDirection = new Direction();
    }

    public synchronized void setDirection(Direction d){
        this.currentDirection = d;
    }

    @Override
    public void run() {
        super.connect();
        while(true){
            if (!this.getCurrentDirection().isNull()) {
                //System.out.println("Sending Move command: "+this.getCurrentDirection().toString());
                try {
                    super.out.writeObject(getCurrentDirection());
                    super.out.flush();
                    Thread.sleep(500);
                } catch (Exception ex) {
                    System.out.println("An error has happened when sending motor command."
                                     + " Connection lost? Reconnecting...");
                    super.connect();
                }
            }
        }
    }
    
    
}