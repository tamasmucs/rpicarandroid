package tmx.rpicarandroid;

import android.graphics.Bitmap;
import android.widget.ImageView;

/**
 * Created by tamas on 2016. 02. 11..
 */
public class SnapshotUpdater implements Runnable {

    private ImageView imageViewCurrentSnapshot;
    private CarManager carManager;

    public SnapshotUpdater(ImageView imageView, CarManager carManager) {
        this.imageViewCurrentSnapshot = imageView;
        this.carManager = carManager;
    }

    private void setImage(){
        imageViewCurrentSnapshot.setImageBitmap(carManager.getCurrentSnapshot());
    }

    @Override
    public void run() {
        Bitmap tempCurrentSnapshot;
        if((tempCurrentSnapshot = carManager.getCurrentSnapshot()) != null){
            imageViewCurrentSnapshot.setImageBitmap(tempCurrentSnapshot);
        }else{
        }
        imageViewCurrentSnapshot.postDelayed(this, 500);
    }
}
