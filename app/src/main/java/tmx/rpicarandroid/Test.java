/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tmx.rpicarandroid;

import tmx.rpicarcommon.Direction;

/**
 *
 * @author tamas
 */
public class Test {
    private static CarManager cManager = new CarManager(MainActivity.rpiAddress);

    public static void testEnvmngr() throws InterruptedException{
        cManager.startFunctionalities();
        while(true){
            System.out.println(cManager.getCurrentEnvironmentData()); 
            Thread.sleep(1000);
        }
    }
          
    public static void testMotor() throws InterruptedException{
        cManager.startFunctionalities();
        while(true){
            cManager.setDirection(new Direction(1, 0));
            Thread.sleep(1500);
            cManager.setDirection(new Direction());
            Thread.sleep(500);
            cManager.setDirection(new Direction(-1, 0));
            Thread.sleep(1500);
            cManager.setDirection(new Direction());
            Thread.sleep(500);
            cManager.setDirection(new Direction(0, -1));
            Thread.sleep(1500); 
            cManager.setDirection(new Direction());
            Thread.sleep(500);
            cManager.setDirection(new Direction(0, 1));
            Thread.sleep(2000);
            cManager.setDirection(new Direction());
            Thread.sleep(500);
        }
    }
}
