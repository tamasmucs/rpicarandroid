package tmx.rpicarandroid;

import tmx.rpicarcommon.CarComponent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import tmx.rpicarcommon.EnvironmentData;
import java.net.InetAddress;


public class EnvironmentManager extends CarComponent implements Runnable{
    private EnvironmentData currentEnvironmentData;
    
    public EnvironmentManager(String rpiAddress) {
        super(rpiAddress, 2176, false);
        this.currentEnvironmentData = new EnvironmentData();
    }

    public synchronized EnvironmentData getCurrentEnvironmentData() {
        return currentEnvironmentData;
    }

    public synchronized void setCurrentEnvironmentData(EnvironmentData currentEnvironmentData) {
        this.currentEnvironmentData = currentEnvironmentData;
    }

    @Override
    public void run() {
        super.connect();
        while(true){
            try {
                this.setCurrentEnvironmentData((EnvironmentData) super.in.readObject());
            } catch (Exception ex) {
                super.connect();
            }
        }
        
    }
}