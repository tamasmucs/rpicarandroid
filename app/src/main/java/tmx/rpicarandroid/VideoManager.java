/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tmx.rpicarandroid;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import java.io.IOException;

import tmx.rpicarcommon.CarComponent;

/**
 *
 * @author tamas
 */
public class VideoManager extends CarComponent implements Runnable{

    private Bitmap currentSnapshot;

    public VideoManager(String rpiAddress, boolean asServer)  {
        super(rpiAddress,2177, asServer);
        Bitmap currentSnapshot;
    }

    public synchronized Bitmap getCurrentSnapshot(){
        return this.currentSnapshot;
    }

    private synchronized void setCurrentSnapshot(Bitmap bitmap){
        this.currentSnapshot = bitmap;
    }

    @Override
    public void run() {
        super.connect();
        while(true){
            try {
                String encodedString = super.in.readUTF();
                byte[] decodedString = Base64.decode(encodedString, Base64.DEFAULT);
                Bitmap bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                if (bitmap != null) setCurrentSnapshot(bitmap);
            } catch (IOException ex) {
                System.out.println("Error at reading picture");
                super.connect();
            }catch(NullPointerException ex){
                //Means that picture data is empty. Let's ignore that for now ...
            }

        }
    }
    
}
