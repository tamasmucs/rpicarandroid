package tmx.rpicarandroid;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView textviewCurrentTemperature;
    private TextView textviewCurrentDistance;
    private CarManager carManager;
    public static final int refreshInterval = 100;
    public static String rpiAddress = "192.168.0.";
    public Handler mHandler = new Handler();

    Button buttonRWD;
    Button buttonFWD;
    Button buttonL;
    Button buttonR;
    ImageView imageViewCurrentSnapshot;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textviewCurrentTemperature = (TextView) findViewById(R.id.textTemperature);
        textviewCurrentDistance = (TextView) findViewById(R.id.textDistance);
        buttonRWD = (Button) findViewById(R.id.buttonRWD);
        buttonFWD = (Button) findViewById(R.id.buttonFWD);
        buttonL = (Button) findViewById(R.id.buttonL);
        buttonR = (Button) findViewById(R.id.buttonR);
        imageViewCurrentSnapshot = (ImageView) findViewById(R.id.imageViewCurrentSnapshot);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.options_menu:
                ShowConnectionSettings();
                return true;
            default:
                System.out.println("e "+item.getItemId());
                return super.onOptionsItemSelected(item);
        }
    }

    private void ShowConnectionSettings() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Title");
        // Set up the input
        final EditText input = new EditText(this);
        input.setText(rpiAddress);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
        builder.setView(input);
        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                setIP(input.getText().toString());
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void setIP(String s) {
        MainActivity.rpiAddress = s;

        //connect to RPi and start receiving data
        carManager = new CarManager(rpiAddress);
        carManager.startFunctionalities();

        //background job: regularly check if any direction is input by the user
        mHandler.post(new DirectionInputChecker(this,buttonRWD,buttonFWD,buttonL,buttonR,this.carManager));

        //background job: update temperature periodically
        textviewCurrentTemperature.post(new TemperatureUpdater(this.textviewCurrentTemperature, this.carManager));

        //background job: update distance periodically
        textviewCurrentDistance.post(new DistanceUpdater(this.textviewCurrentDistance, this.carManager));

        //background job: update image periodically.
        imageViewCurrentSnapshot.post(new SnapshotUpdater(imageViewCurrentSnapshot, carManager));
    }


}
