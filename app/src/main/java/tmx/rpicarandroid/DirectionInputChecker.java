package tmx.rpicarandroid;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.widget.Button;

import tmx.rpicarcommon.Direction;

/**
 * Created by tamas on 2016. 02. 09..
 */
public class DirectionInputChecker implements Runnable {

    Button rwdButton;
    Button fwdButton;
    Button lButton;
    Button rButton;

    MainActivity owner;
    CarManager carManager;

    public DirectionInputChecker(MainActivity owner, Button rwdButton, Button fwdButton,Button lButton, Button rButton, CarManager carManager) {
        this.rwdButton = rwdButton;
        this.fwdButton = fwdButton;
        this.lButton = lButton;
        this.rButton = rButton;
        this.owner = owner;
        this.carManager = carManager;
    }

    @Override
    public void run() {
        //collect vertical input data
        int v = 0;
        if (fwdButton.isPressed()) {
            v = 1;
        } else if (rwdButton.isPressed()) {
            v = -1;
        }else {
            v=0;
        }

        //check horizontal input
        int h = 0;
        if (lButton.isPressed()) {
            h = -1;
        } else if (rButton.isPressed()) {
            h = 1;
        }else {
            h=0;
        }

        Direction d = new Direction(v, h);

        carManager.setDirection(new Direction(v, h));
        owner.mHandler.postDelayed(this, MainActivity.refreshInterval);
    }

}


