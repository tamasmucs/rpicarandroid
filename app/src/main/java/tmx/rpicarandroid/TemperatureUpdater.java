package tmx.rpicarandroid;

import android.widget.TextView;

/**
 * Created by tamas on 2016. 02. 07..
 */
public class TemperatureUpdater implements Runnable {

    private TextView tempTextView;
    private double currentTemp = 0;
    private CarManager cManager;

    public TemperatureUpdater(TextView tempTextView, CarManager cManager) {
        this.tempTextView = tempTextView;
        this.cManager = cManager;
    }

    @Override
    public void run(){
        updateCurrentTemperature();
        String temperatureText= String.format("%.2f °C",currentTemp );
        tempTextView.setText(temperatureText);
        tempTextView.postDelayed(this, 1000);
    }

    private void updateCurrentTemperature(){
        try {
            this.currentTemp = cManager.getCurrentEnvironmentData().getTemperature();
        }catch(NullPointerException ex){
            this.currentTemp = 0;
            System.out.println("NullPointer @ setting temperature.");
        }
    }


}
